# Nanopore-based native RNA sequencing provides insights into prokaryotic transcription, operon structures, rRNA transcription, splicing and RNA modifications 

###  **Authors:** Felix Grünberger, ..., Jörg Soppa, Sebastién Ferreira-Cerca, Winfried Hausner & Dina Grohmann


**********

**Abstract:** 

**********

## Information about this repository:  

This is the repository for the manuscript "Nanopore-based native RNA sequencing provides insights into prokaryotic transcription, operon structures, rRNA transcription, splicing and RNA modifications"

### **Original fastq files:**
The original Fastq files were submitted to the NCBI sequence read archive under BioProject accession number XXX.


### **File Description:**


**********

This project is under the general MIT License.

**********